<h2>Projeto Desafio Digesto Tutorial</h2>

<h3>Bibliotecas Necessárias:</h3>

<ul>
<li>BeautifulSoup</li>
<li>Pandas</li>
<li>Requests</li>
<li>Re</li>
</ul>

<h3>Introdução</h3>
<p> Essa aplicação tem como objetivo coletar dados da tabela SSD Cloud Instances do site <a href="https://www.vultr.com/products/cloud-compute/#pricing">Vultr</a> e a tabela Basic Droplets do site <a href="https://www.digitalocean.com/pricing/">Digital Ocean</a> utilizando técnicas de web scraping. A aplicação também é capaz de gerar uma escrita no console (print), exportar os resultados em um arquivo .json ou em um arquivo csv. Os resultados ficarão salvos nas pasta Reports. </p>

<h3>Iniciando:</h3>

<p> Para executar essa aplicação execute o arquivo Crawler.py localizado na pasta WebCrawler. É necessário indicar a saída desejada marcando um dos parâmetros abaixo como True :</p>

<p>
  <strong>exibe_resultado_console:</strong> Define a saída do código para a função <i>print()</i> no console da IDE ou terminal.
</p>
<p>
  <strong>salvar_json:</strong> Define a saída do código para salvar o resultado em um arquivo .json.
</p>
<p>
  <strong>salvar_csv:</strong> Define a saída do código  para salvar o resultado em um arquivo .csv.
</p>

<h4>
  Exemplo:
</h4>
<p>
  No exemplo abaixo o crawler irá gerar uma saída no console e irá gerar um arquivo .json.
</p>
<pre><code>
if __name__ == '__main__':
    classe = Crawler()
    classe.exibe_resultado_console = True
    classe.salvar_json = True
    classe.salvar_csv = False</code></pre>

<p>Essa aplicação também pode ser executada por linha de comando. Para isso deve ser executado o arquivo Crawler.py localizado na pasta WebCrawler e indicar como True ou False no terminal as saidas que deseja utilizar.</p>

<h4>
  Exemplo:
</h4>
<p>
  No exemplo abaixo o crawler irá gerar uma saída no console e irá gerar um arquivo .json.
</p>
<p>
  <code>python Crawler.py True True False</code>
</p>

<h3>
  Métodos da Classe
</h3>

<p>
  <strong>coleta_pagina_vultr:</strong> Esse método irá realizar a coleta dos dados contidos na tabela SSD Cloud Instances hospedada no site <a href="https://www.vultr.com/products/cloud-compute/#pricing">Vultr</a> e irá gerar um dataframe utilizando o pandas com os dados dessa tabela.
</p>
<p>
  <strong>coleta_pagina_digital_ocean:</strong> Esse método irá realizar a coleta dos dados contidos na tabela Basic Droplets hospedada no site  <a href="https://www.digitalocean.com/pricing/">Digital Ocean</a> e irá gerar um dataframe utilizando o pandas com os dados dessa tabela.
</p>
<p>
  <strong>exporta_resultados:</strong> Esse método irá realizar a saída definida pelo usuário que podem ser escrever no console (print), salvar em um arquivo .json ou salvar em um arquivo .csv
</p>
<p>
  <strong>realiza_requisicao:</strong> Esse método irá realizar a requisição na url solicitada realizando alguns tratamentos na requisição e irá retornar um objeto request
</p>

