import requests,re,time,sys
import pandas as pd
from bs4 import BeautifulSoup as bs
from distutils.util import strtobool

class Crawler():

    def __init__(self):
        self.sessao = requests.Session()
        self.url_vultr = 'https://www.vultr.com/products/cloud-compute/#pricing'
        self.url_digital_ocean = 'https://www.digitalocean.com/pricing/'
        self.exibe_resultado_console = False
        self.salvar_json = False
        self.salvar_csv = False

    def coleta_pagina_vultr(self):
        #Realiza requisição na url de destino
        try:
            pagina = self.realiza_requisicao(self.sessao,self.url_vultr)
        except Exception:
            print("Página não está disponível no momento")
            return False

        #Cria uma instância da biblioteca beautfulsoup com o objeto página do request
        soup = bs(pagina.text, 'html5lib')

        #Coleta a tabela SSD Cloud Instances
        tabela = soup.find_all('div', {'class': 'pt__row-content'})

        #Dicionário que será adicionado ao DataFrame
        dicVultr = {}

        # Criação do Dataframe Pandas com as colunas indicadas pré definidas
        colunas = ['Storage', 'CPU', 'Memory', 'Bandwidth', 'Price']
        dfVultr = pd.DataFrame(columns=colunas)

        for linha in tabela:
            # Faço uma iteração sobre a tabela SSD Cloud pegando linha por linha a partir do indice 1, removendo a coluna Geekbench Score
            for count,item in enumerate(linha.find_all('div', {'class': 'pt__cell'})[1:]):
                texto = item.text
                texto = re.sub('\t|\n|Bandwidth(IPv6)?','',texto)#Limpeza da coluna Bandwidth

                # Separando o preço em /mo do preço em /hr
                if "$" in texto:
                    texto = '$' + texto.split('$')[1]

                # Preenchendo o dicionário que será adicionado ao dataframe utilizando a posição das colunas criadas para o pandas como chave no dicionário
                #Isso é possível pela ordem das colunas seguirem iguais as da tabela do site
                dicVultr[colunas[count]] = texto

            #append do dataframe
            dfVultr = dfVultr.append(dicVultr,ignore_index=True)

        #Chama o método responsável por realizar a saída da aplicação passando o dataframe como parâmetro
        self.exporta_resultados(dfVultr,'SSD_Cloud_Instances')

    def coleta_pagina_digital_ocean(self):
        # Realiza requisição na url de destino
        try:
            pagina = self.realiza_requisicao(self.sessao, self.url_digital_ocean)
        except Exception:
            print("Página não está disponível no momento")
            return False

        # Cria uma instância da biblioteca beautfulsoup com o objeto página do request
        soup = bs(pagina.text, 'html5lib')

        #Cria um dataframe vazio com as colunas já pré determinadas
        dfDigitalOcean = pd.DataFrame(columns=['Price', 'Memory', 'CPU', 'Storage', 'Bandwidth'])

        #Dicionário que irá armazenar os valores para serem incluídos no dataframe
        dicDigitalOcean = {}

        #Irá coletar a tabela Basic droplets inteira
        tabela = soup.find_all('li', {'class':'priceBoxItem'})

        #Irá parsear todas as colunas da tabela Basic Droplets
        for linha in tabela:
            #Irá coletar o primeiro valor em /mo
            dicDigitalOcean['Price'] = linha.find_all('div')[1].text

            #Esse for irá iterar todos os itens dentro da coluna, construídos com a tag li
            for item in linha.find_all('li'):
                #Coletar CPU e Memória
                if 'CPU' in item.text:
                    dicDigitalOcean['Memory'] = item.text.split('/')[0]
                    dicDigitalOcean['CPU'] = item.text.split('/')[1]
                #Coletar o tamanho do storage
                if 'Disk' in item.text:
                    dicDigitalOcean['Storage'] = item.text
                #Coletar a taxa de transferência
                if 'transfer' in item.text:
                    dicDigitalOcean['Bandwidth'] = item.text
            #com o dicionário construído irá realizar append no dataframe
            dfDigitalOcean = dfDigitalOcean.append(dicDigitalOcean, ignore_index=True)

        #Chama o método responsável por realizar a saída da aplicação passando o dataframe como parâmetro
        self.exporta_resultados(dfDigitalOcean,'Basic_Droplets')

    #Método que exporta os resultados das coletas
    def exporta_resultados(self,data,nome_arquivo):
        print('#---------------------------------------------------------------------#')
        if self.exibe_resultado_console:
            print(data.to_string(index=False))
            print()
        if self.salvar_json:
            data.to_json('../Reports/'+nome_arquivo+'.json')
            print('Arquivo JSON criado no diretório: Reports como '+nome_arquivo+'.json')
        if self.salvar_csv:
            data.to_csv('../Reports/'+nome_arquivo+'.csv',index=False,sep =';')
            print('Arquivo CSV criado no diretório: Reports como '+nome_arquivo+'.csv')
        print('#---------------------------------------------------------------------#')

    #Método para realizar a requisição dado uma url
    def realiza_requisicao(self, sessao, url):
        tentativas = 4
        conseguiu = False
        pagina = None

        while tentativas >= 0 and not conseguiu:
            try:#Tenta realizar a requisição em 4 tentativas
                pagina = sessao.get(url, timeout=300)
                if not pagina:
                    conseguiu = False
                else:
                    conseguiu = True
            except:
                time.sleep(10)
                tentativas -= 1
        return pagina

if __name__ == '__main__':
    classe = Crawler()
    #Trecho para execução via linha de comando
    if len(sys.argv) > 1:
        classe.exibe_resultado_console = bool(strtobool(sys.argv[1]))
        classe.salvar_json = bool(strtobool(sys.argv[2]))
        classe.salvar_csv = bool(strtobool(sys.argv[3]))
    #Trecho para execução via IDE.Indica como True ou False os parâmetros que deseja executar
    else:
        classe.exibe_resultado_console = False
        classe.salvar_json = False
        classe.salvar_csv = False

    classe.coleta_pagina_vultr()
    classe.coleta_pagina_digital_ocean()

